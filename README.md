## Introduction
**How to run app :**
```
php artisan migrate
php artisan db:seed
php artisan serve
```

**Available user from db seeder :**

| Role | Email |
| ------ | ------ |
| Admin | admin@admin.com |
| Apoteker | apoteker@admin.com |
| Dokter | dokter@admin.com |
| Pasien | pasien@admin.com |

Password for all user : **password**

This site was built using [Laravel](https://laravel.com/).

## Things to do
**- Layout**
- [x] Homepage
- [ ] Login Page
- [ ] Register Page

**- Admin Section**
- [ ] Finish my changes
- [ ] Push my commits to GitHub
- [ ] Open a pull request

**- Dokter Section**
- [ ] Finish my changes
- [ ] Push my commits to GitHub
- [ ] Open a pull request

**- Pasien Section**
- [ ] Finish my changes
- [ ] Push my commits to GitHub
- [ ] Open a pull request